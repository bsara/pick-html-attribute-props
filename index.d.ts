/**!
 * pick-html-attribute-props v1.4.1
 *
 * Copyright (c) 2024, Brandon D. Sara (https://bsara.dev/)
 * Licensed under the ISC license (https://gitlab.com/bsara/pick-html-attribute-props/blob/master/LICENSE)
 */

import { FormHtmlAttrNames, GlobalHtmlAttrNames, InputHtmlAttrNames, TextAreaHtmlAttrNames } from './attr-names';



/**
 * Creates a new object composed of properties "picked" from `obj` that are valid HTML
 * Form attributes or are event handlers _(unless specified otherwise in `options`)_
 * found in the given `obj`. If `obj` is not defined, then an empty object is returned.
 *
 * @param {?object}  obj - Object to be used when searching for HTML attribute values.
 *
 * @param {?object}  [options]
 * @param {?boolean} [options.omitEventHandlers = false] - when `true`, event handlers
 *                                                         will be omitted from the
 *                                                         returned object.
 *
 * @returns {!object} A new object composed of only those properties that were
 *                    determined to be valid HTML Form attributes or are event handlers
 *                    _(unless `options.omitEventHandlers` is `true`, then all event
 *                    handlers will be omitted form the returned object)_. If none are
 *                    found, then an empty object is returned.
 */
export function pickFormHtmlAttrProps(obj: Record<string, any> | null, options?: PickAttrPropsOptions | null): PickedFormHtmlAttrProps;

export type PickedFormHtmlAttrProps = {
  [key in FormHtmlAttrNames[number] | DataAriaOrEventHandlerName]: any | null;
};



/**
 * Creates a new object composed of properties "picked" from `obj` that are valid HTML
 * attributes that can be supplied to any HTML element or are event handlers _(unless
 * specified otherwise in `options`)_ found in the given `obj`. If `obj` is not defined,
 * then an empty object is returned.
 *
 * @param {?object}  obj - Object to be used when searching for HTML attribute values.
 *
 * @param {?object}  [options]
 * @param {?boolean} [options.omitEventHandlers = false] - when `true`, event handlers
 *                                                         will be omitted from the
 *                                                         returned object.
 *
 * @returns {!object} A new object composed of only those properties that were determined
 *                    to be valid, globally applicable HTML attributes or are event
 *                    handlers _(unless `options.omitEventHandlers` is `true`, then all
 *                    event handlers will be omitted form the returned object)_. If none
 *                    are found, then an empty object is returned.
 */
export function pickGlobalHtmlAttrProps(obj: Record<string, any> | null, options?: PickAttrPropsOptions | null): PickedGlobalHtmlAttrProps;

export type PickedGlobalHtmlAttrProps = {
  [key in GlobalHtmlAttrNames[number] | DataAriaOrEventHandlerName]: any | null;
};



/**
 * Creates a new object composed of properties "picked" from `obj` that are valid HTML
 * Input attributes or are event handlers _(unless specified otherwise in `options`)_
 * found in the given `obj`. If `obj` is not defined, then an empty object is returned.
 *
 * @param {?object}  obj - Object to be used when searching for HTML attribute values.
 *
 * @param {?object}  [options]
 * @param {?boolean} [options.omitEventHandlers = false] - when `true`, event handlers
 *                                                         will be omitted from the
 *                                                         returned object.
 *
 * @returns {!object} A new object composed of only those properties that were
 *                    determined to be valid HTML Input attributes or are event handlers
 *                    _(unless `options.omitEventHandlers` is `true`, then all event
 *                    handlers will be omitted form the returned object)_. If none are
 *                    found, then an empty object is returned.
 */
export function pickInputHtmlAttrProps(obj: Record<string, any> | null, options?: PickAttrPropsOptions | null): PickedInputHtmlAttrProps;

export type PickedInputHtmlAttrProps = {
  [key in InputHtmlAttrNames[number] | DataAriaOrEventHandlerName]: any | null;
};



/**
 * Creates a new object composed of properties "picked" from `obj` that are valid HTML
 * TextArea attributes or are event handlers _(unless specified otherwise in `options`)_
 * found in the given `obj`. If `obj` is not defined, then an empty object is returned.
 *
 * @param {?object}  obj - Object to be used when searching for HTML attribute values.
 *
 * @param {?object}  [options]
 * @param {?boolean} [options.omitEventHandlers = false] - when `true`, event handlers
 *                                                         will be omitted from the
 *                                                         returned object.
 *
 * @returns {!object} A new object composed of only those properties that were
 *                    determined to be valid HTML TextArea attributes or are event
 *                    handlers _(unless `options.omitEventHandlers` is `true`, then all
 *                    event handlers will be omitted form the returned object)_. If none
 *                    are found, then an empty object is returned.
 */
export function pickTextAreaHtmlAttrProps(obj: Record<string, any> | null, options?: PickAttrPropsOptions | null): PickedTextAreaHtmlAttrProps;

export type PickedTextAreaHtmlAttrProps = {
  [key in TextAreaHtmlAttrNames[number] | DataAriaOrEventHandlerName]: any | null;
};



export type PickAttrPropsOptions = {
  /**
   * When truthy, event handlers will be omitted from the returned object.
   */
  omitEventHandlers?: any | null
};



export type DataAriaOrEventHandlerName = StartsWith<'data-'> | StartsWith<'aria-'> | StartsWith<'on'>;



type StartsWith<Str extends string> = Str extends `${Str}${infer _}` ? Str : never;
