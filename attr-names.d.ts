/**!
 * pick-html-attribute-props v1.4.1
 *
 * Copyright (c) 2024, Brandon D. Sara (https://bsara.dev/)
 * Licensed under the ISC license (https://gitlab.com/bsara/pick-html-attribute-props/blob/master/LICENSE)
 */



export function getFormHtmlAttrNames(): FormHtmlAttrNames;

export type FormHtmlAttrNames = [
  ...GlobalHtmlAttrNames,

  'accept',
  'acceptCharset',
  'action',
  'autoComplete',
  'encType',
  'method',
  'name',
  'noValidate',
  'rel',
  'target'
];



export function getGlobalHtmlAttrNames(): GlobalHtmlAttrNames;

export type GlobalHtmlAttrNames = [
  'accessKey',
  'autoCapitalize',
  'className',
  'contentEditable',
  'contextMenu',
  'dir',
  'draggable',
  'hidden',
  'id',
  'is',
  'itemID',
  'itemProp',
  'itemRef',
  'itemScope',
  'itemType',
  'lang',
  'slot',
  'spellCheck',
  'style',
  'tabIndex',
  'title',
  'translate'
];



export function getInputHtmlAttrNames(): InputHtmlAttrNames;

export type InputHtmlAttrNames = [
  ...GlobalHtmlAttrNames,

  'accept',
  'autoComplete',
  'autoFocus',
  'capture',
  'defaultValue',
  'disabled',
  'form',
  'formAction',
  'formEncType',
  'formMethod',
  'formNoValidate',
  'formTarget',
  'inputMode',
  'list',
  'max',
  'maxLength',
  'min',
  'minLength',
  'multiple',
  'name',
  'pattern',
  'placeholder',
  'readOnly',
  'required',
  'selectionDirection',
  'selectionEnd',
  'selectionStart',
  'src',
  'step',
  'type',
  'value',
  'wrap'
];


export function getTextAreaHtmlAttrNames(): TextAreaHtmlAttrNames;

export type TextAreaHtmlAttrNames = [
  ...GlobalHtmlAttrNames,

  'cols',
  'defaultValue',
  'disabled',
  'form',
  'maxLength',
  'minLength',
  'name',
  'placeholder',
  'readOnly',
  'required',
  'rows',
  'value',
  'wrap'
];
