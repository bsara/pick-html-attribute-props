// eslint-disable-next-line spaced-comment
/**!
 * pick-html-attribute-props v1.4.1
 *
 * Copyright (c) 2024, Brandon D. Sara (https://bsara.dev/)
 * Licensed under the ISC license (https://gitlab.com/bsara/pick-html-attribute-props/blob/master/LICENSE)
 */

let FORM_HTML_ATTRIBUTE_NAMES;

/** @returns {string[]} */
export function getFormHtmlAttrNames() {
  if (!FORM_HTML_ATTRIBUTE_NAMES) {
    FORM_HTML_ATTRIBUTE_NAMES = [
      ...getGlobalHtmlAttrNames(),

      'accept',
      'acceptCharset',
      'action',
      'autoComplete',
      'encType',
      'method',
      'name',
      'noValidate',
      'rel',
      'target'
    ];

    Object.freeze(FORM_HTML_ATTRIBUTE_NAMES);
  }

  return FORM_HTML_ATTRIBUTE_NAMES;
}



let GLOBAL_HTML_ATTRIBUTE_NAMES;

/** @returns {string[]} */
export function getGlobalHtmlAttrNames() {
  if (!GLOBAL_HTML_ATTRIBUTE_NAMES) {
    GLOBAL_HTML_ATTRIBUTE_NAMES = [
      'accessKey',
      'autoCapitalize',
      'className',
      'contentEditable',
      'contextMenu',
      'dir',
      'draggable',
      'dropzone',
      'hidden',
      'id',
      'is',
      'itemID',
      'itemProp',
      'itemRef',
      'itemScope',
      'itemType',
      'lang',
      'slot',
      'spellCheck',
      'style',
      'tabIndex',
      'title',
      'translate'
    ];

    Object.freeze(GLOBAL_HTML_ATTRIBUTE_NAMES);
  }

  return GLOBAL_HTML_ATTRIBUTE_NAMES;
}



let INPUT_HTML_ATTRIBUTE_NAMES;

/** @returns {string[]} */
export function getInputHtmlAttrNames() {
  if (!INPUT_HTML_ATTRIBUTE_NAMES) {
    INPUT_HTML_ATTRIBUTE_NAMES = [
      ...getGlobalHtmlAttrNames(),

      'accept',
      'autoComplete',
      'autoFocus',
      'capture',
      'defaultValue',
      'disabled',
      'form',
      'formAction',
      'formEncType',
      'formMethod',
      'formNoValidate',
      'formTarget',
      'inputMode',
      'list',
      'max',
      'maxLength',
      'min',
      'minLength',
      'multiple',
      'name',
      'pattern',
      'placeholder',
      'readOnly',
      'required',
      'selectionDirection',
      'selectionEnd',
      'selectionStart',
      'src',
      'step',
      'type',
      'value',
      'wrap'
    ];

    Object.freeze(INPUT_HTML_ATTRIBUTE_NAMES);
  }

  return INPUT_HTML_ATTRIBUTE_NAMES;
}



let TEXTAREA_HTML_ATTRIBUTE_NAMES;

/** @returns {string[]} */
export function getTextAreaHtmlAttrNames() {
  if (!TEXTAREA_HTML_ATTRIBUTE_NAMES) {
    TEXTAREA_HTML_ATTRIBUTE_NAMES = [
      ...getGlobalHtmlAttrNames(),

      'cols',
      'defaultValue',
      'disabled',
      'form',
      'maxLength',
      'minLength',
      'name',
      'placeholder',
      'readOnly',
      'required',
      'rows',
      'value',
      'wrap'
    ];

    Object.freeze(TEXTAREA_HTML_ATTRIBUTE_NAMES);
  }

  return TEXTAREA_HTML_ATTRIBUTE_NAMES;
}
