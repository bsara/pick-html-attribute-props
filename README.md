# pick-html-attribute-props [![NPM Package](https://img.shields.io/npm/v/pick-html-attribute-props.svg?style=flat-square)][npm]

![ISC License](https://img.shields.io/badge/license-ISC-blue.svg?style=flat-square)


> "Pickers" and other helper functions used to filter standard HTML attribute values from
> an object.
>
> _NOTE: This library was created with the intent to be used by React components, but
>        React is not required to use this library._


[Changelog](https://gitlab.com/bsara/pick-html-attribute-props/blob/master/CHANGELOG.md)


## Install

```bash
$ npm i --save pick-html-attribute-props
```


## Usage

```jsx
import React from 'react';
import {
  pickFormHtmlAttrProps,
  pickGlobalHtmlAttrProps,
  pickInputHtmlAttrProps,
  pickTextAreaHtmlAttrProps
} from 'pick-html-attribute-props';

export function MyComponent(props) {
  return (
    <div {...pickGlobalHtmlAttrProps(props)}>
      {/* ... */}
    </div>
  );
}

export function MyFormComponent(props) {
  return (
    <form {...pickFormHtmlAttrProps(props)} {/* ... */}>
      {/* ... */}
    </form>
  );
}

export function MyInputComponent(props) {
  return (
    <input {...pickInputHtmlAttrProps(props)} {/* ... */} />
  );
}

export function MyTextAreaComponent(props) {
  return (
    <textarea {...pickTextAreaHtmlAttrProps(props)}>
      {/* ... */}
    </textarea>
  );
}
```


<br/>


## API

#### **pickFormHtmlAttrProps(obj[, options])**

Creates a new object composed of properties "picked" from `obj` that are [valid HTML Form
attributes][form-html-attrs] or are event handlers _(unless specified otherwise in
`options`)_ found in the given `obj`. If `obj` is not defined, then an empty object is
returned.

**Parameters**
- **obj** `?Object` - object to be used when searching for HTML attribute values.
- **[options.omitEventHandlers = false]** `?Boolean` - when `true`, event handlers will
                                                       be omitted from the returned object.

**Returns** `!Object`
- A new object composed of only those properties that were determined to be valid HTML
  Form attributes or are event handlers _(unless `options.omitEventHandlers` is `true`,
  then all event handlers will be omitted form the returned object)_. If none are found,
  then an empty object is returned.

**Examples**

```jsx
import { pickFormHtmlAttrProps } from 'pick-html-attribute-props';

pickFormHtmlAttrProps(null); // returns `{}`
pickFormHtmlAttrProps({
  id: 42,
  type: 'text',
  defaultValue: 'Fish fingers and custard',
  className: 'my-class-name',
  onChange: handleChange,
  "data-id": "myDataId",
  "aria-blah": "myAriaProp"
}); // returns a new object that contains all of the properties in the original object

pickFormHtmlAttrProps({ onChange: handleChange }, { omitEventHandlers: true }); // returns `{}` (event handlers are being ommitted from result)
pickFormHtmlAttrProps({ id: 42, name: "Doctor Who", blah: 78 });                // returns `{ id: 42, name: "Doctor Who" }` ("blah" is not a valid HTML Form attribute)
```

---

#### **pickGlobalHtmlAttrProps(obj[, options])**

Creates a new object composed of properties "picked" from `obj` that are [valid HTML
attributes that can be supplied to any HTML element][global-html-attrs] or are
event handlers _(unless specified otherwise in `options`)_ found in the given `obj`. If
`obj` is not defined, then an empty object is returned.

**Parameters**
- **obj** `?Object` - object to be used when searching for HTML attribute values.
- **[options.omitEventHandlers = false]** `?Boolean` - when `true`, event handlers will
                                                       be omitted from the returned object.

**Returns** `!Object`
- A new object composed of only those properties that were determined to be valid,
  globally applicable HTML attributes or are event handlers _(unless
  `options.omitEventHandlers` is `true`, then all event handlers will be omitted form the
  returned object)_. If none are found, then an empty object is returned.

**Examples**

```jsx
import { pickGlobalHtmlAttrProps } from 'pick-html-attribute-props';

pickGlobalHtmlAttrProps(null); // returns `{}`
pickGlobalHtmlAttrProps({
  id: 42,
  className: 'my-class-name',
  onChange: handleChange,
  "data-id": "myDataId",
  "aria-blah": "myAriaProp"
}); // returns a new object that contains all of the properties in the original object

pickGlobalHtmlAttrProps({ onChange: handleChange }, { omitEventHandlers: true }); // returns `{}` (event handlers are being ommitted from result)
pickGlobalHtmlAttrProps({ id: 42, name: "Doctor Who", blah: 78 });                // returns `{ id: 42 }` ("name" & "blah" are not a valid HTML attribute for all HTML elements)
```

---

#### **pickInputHtmlAttrProps(obj[, options])**

Creates a new object composed of properties "picked" from `obj` that are [valid HTML Input
attributes][input-html-attrs] or are event handlers _(unless specified otherwise in
`options`)_ found in the given `obj`. If `obj` is not defined, then an empty object is
returned.

**Parameters**
- **obj** `?Object` - object to be used when searching for HTML attribute values.
- **[options.omitEventHandlers = false]** `?Boolean` - when `true`, event handlers will
                                                       be omitted from the returned object.

**Returns** `!Object`
- A new object composed of only those properties that were determined to be valid HTML
  Input attributes or are event handlers _(unless `options.omitEventHandlers` is `true`,
  then all event handlers will be omitted form the returned object)_. If none are found,
  then an empty object is returned.

**Examples**

```jsx
import { pickInputHtmlAttrProps } from 'pick-html-attribute-props';

pickInputHtmlAttrProps(null); // returns `{}`
pickInputHtmlAttrProps({
  id: 42,
  type: 'text',
  defaultValue: 'Fish fingers and custard',
  className: 'my-class-name',
  onChange: handleChange,
  "data-id": "myDataId",
  "aria-blah": "myAriaProp"
}); // returns a new object that contains all of the properties in the original object

pickInputHtmlAttrProps({ onChange: handleChange }, { omitEventHandlers: true }); // returns `{}` (event handlers are being ommitted from result)
pickInputHtmlAttrProps({ id: 42, name: "Doctor Who", blah: 78 });                // returns `{ id: 42, name: "Doctor Who" }` ("blah" is not a valid HTML Input attribute)
```

---

#### **pickTextAreaHtmlAttrProps(obj[, options])**

Creates a new object composed of properties "picked" from `obj` that are [valid HTML
TextArea attributes][textarea-html-attrs] and event handlers _(unless specified otherwise
in `options`)_ found in the given `obj`. If `obj` is not defined, then an empty object is
returned.

**Parameters**
- **obj** `?Object` - object to be used when searching for HTML attribute values.
- **[options.omitEventHandlers = false]** `?Boolean` - when `true`, event handlers will
                                                       be omitted from the returned object.

**Returns** `!Object`
- A new object composed of only those properties that were determined to be valid HTML
  TextArea attributes or are event handlers _(unless `options.omitEventHandlers` is
  `true`, then all event handlers will be omitted form the returned object)_. If none are
  found, then an empty object is returned.

**Examples**

```jsx
import { pickTextAreaHtmlAttrProps } from 'pick-html-attribute-props';

pickTextAreaHtmlAttrProps(null); // returns `{}`
pickTextAreaHtmlAttrProps({
  id: 42,
  defaultValue: 'Fish fingers and custard',
  className: 'my-class-name',
  onChange: handleChange,
  "data-id": "myDataId",
  "aria-blah": "myAriaProp"
}); // returns a new object that contains all of the properties in the original object

pickTextAreaHtmlAttrProps({ onChange: handleChange }, { omitEventHandlers: true }); // returns `{}` (event handlers are being ommitted from result)
pickTextAreaHtmlAttrProps({ id: 42, name: "Doctor Who", blah: 78 });                // returns `{ id: 42, name: "Doctor Who" }` ("blah" is not a valid HTML TextArea attribute)
```


<br/>


## Attribute Name Getters

#### **getFormlHtmlAttrNames()**

Returns a list of attribute names that are valid for [an HTML Form element][form-html-attrs].

> NOTE: A list of attribute names returned can be found in [`attr-names.js`](https://gitlab.com/bsara/pick-html-attribute-props/-/blob/master/attr-names.js#L15-26).

**Example**
```javascript
import { getFormHtmlAttrNames } from 'pick-html-attribute-props/attr-names';

console.log(getFormHtmlAttrNames()); // list of attribute names that are valid for an HTML Form element
```

---

#### **getGlobalHtmlAttrNames()**

Returns a list of attribute names that are valid for [any HTML element][global-html-attrs].

> NOTE: A list of attribute names returned can be found in [`attr-names.js`](https://gitlab.com/bsara/pick-html-attribute-props/-/blob/master/attr-names.js#L43-65).

**Example**
```javascript
import { getGlobalHtmlAttrNames } from 'pick-html-attribute-props/attr-names';

console.log(getGlobalHtmlAttrNames()); // list of attribute names that are valid for any HTML element
```

---

#### **getInputHtmlAttrNames()**

Returns a list of attribute names that are valid for [an HTML Input element][input-html-attrs].

> NOTE: A list of attribute names returned can be found in [`attr-names.js`](https://gitlab.com/bsara/pick-html-attribute-props/-/blob/master/attr-names.js#L82-115).

**Example**
```javascript
import { getInputHtmlAttrNames } from 'pick-html-attribute-props/attr-names';

console.log(getInputHtmlAttrNames()); // list of attribute names that are valid for an HTML Input element
```

---

#### **getTextAreaHtmlAttrNames()**

Returns a list of attribute names that are valid for [an HTML TextArea element][textarea-html-attrs].

> NOTE: A list of attribute names returned can be found in [`attr-names.js`](https://gitlab.com/bsara/pick-html-attribute-props/-/blob/master/attr-names.js#L132-146).

**Example**
```javascript
import { getTextAreaHtmlAttrNames } from 'pick-html-attribute-props/attr-names';

console.log(getTextAreaHtmlAttrNames()); // list of attribute names that are valid for an HTML TextArea element
```


<br/>


## Prop Name Predicates

#### **propertyIsDataOrAriaAttr(propName)**

Determines if the given `propName` represents a "data" or "aria" attribute.

**Parameters**
- **propName** `?String` - the name of the property to check.

**Returns** `!Boolean`
- `true` if `propName` is a defined `String` and begins with begins with `data-` or
  `aria-`; otherwise, `false`.

**Examples**
```javascript
import { propertyIsDataOrAriaAttr } from 'pick-html-attribute-props/prop-name-predicates';

console.log(propertyIsDataOrAriaAttr("data-blah")) // true
console.log(propertyIsDataOrAriaAttr("aria-blah")) // true

console.log(propertyIsDataOrAriaAttr(null))       // false (parameter isn't a String)
console.log(propertyIsDataOrAriaAttr("datablah")) // false (doesn't start with "data-")
console.log(propertyIsDataOrAriaAttr("ariablah")) // false (doesn't start with "aria-")
```

---

#### **propertyIsDataOrAriaAttrOrEventHandler(propName)**

Determines if the given `propName` represents an event handler, a "data" attribute, or an
"aria" attribute.

**Parameters**
- **propName** `?String` - the name of the property to check.

**Returns** `!Boolean`
- `true` if `propName` is a defined `String` and begins with `on` followed by a
  capitalized letter, begins with `data-`, or begins with `aria-`; otherwise, `false`.

**Examples**
```javascript
import { propertyIsDataOrAriaAttrOrEventHandler } from 'pick-html-attribute-props/prop-name-predicates';

console.log(propertyIsDataOrAriaAttrOrEventHandler("onChange")); // true
console.log(propertyIsDataOrAriaAttrOrEventHandler("onBlah"));   // true

console.log(propertyIsDataOrAriaAttrOrEventHandler("data-blah")) // true
console.log(propertyIsDataOrAriaAttrOrEventHandler("aria-blah")) // true

console.log(propertyIsDataOrAriaAttrOrEventHandler(null))            // false (parameter isn't a String)
console.log(propertyIsDataOrAriaAttrOrEventHandler("onchange"));     // false ("on" is not followed by a capitalized letter)
console.log(propertyIsDataOrAriaAttrOrEventHandler("handleChange")); // false (doesn't start with "on")
console.log(propertyIsDataOrAriaAttrOrEventHandler("datablah"))      // false (doesn't start with "data-")
console.log(propertyIsDataOrAriaAttrOrEventHandler("ariablah"))      // false (doesn't start with "aria-")
```

---

#### **propertyIsEventHandler(propName)**

Determines if the given `propName` **likely** represents an event handler.

**Parameters**
- **propName** `?String` - the name of the property to check.

**Returns** `!Boolean`
- `true` if `propName` is a defined `String` and begins with `on` followed by a
  capitalized letter; otherwise, `false`.

**Examples**
```javascript
import { propertyIsEventHandler } from 'pick-html-attribute-props/prop-name-predicates';

console.log(propertyIsEventHandler("onChange")); // true
console.log(propertyIsEventHandler("onBlah"));   // true

console.log(propertyIsEventHandler(null))            // false (parameter isn't a String)
console.log(propertyIsEventHandler("onchange"));     // false ("on" is not followed by a capitalized letter)
console.log(propertyIsEventHandler("handleChange")); // false (doesn't start with "on")
```


<br/>
<br/>


# License

ISC License (ISC)

Copyright (c) 2024, Brandon D. Sara (https://bsara.dev/)

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.



[license]: https://gitlab.com/bsara/pick-html-attribute-props/blob/master/LICENSE "License"
[npm]:     https://www.npmjs.com/package/pick-html-attribute-props                "NPM Package: pick-html-attribute-props"

[form-html-attrs]:     https://developer.mozilla.org/en-US/docs/Web/HTML/Element/form#attributes     "Form HTML Attributes"
[global-html-attrs]:   https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes           "Global HTML Attributes"
[input-html-attrs]:    https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#Attributes    "Input HTML Attributes"
[textarea-html-attrs]: https://developer.mozilla.org/en-US/docs/Web/HTML/Element/textarea#Attributes "TextArea HTML Attributes"
