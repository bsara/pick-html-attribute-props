// eslint-disable-next-line spaced-comment
/**!
 * pick-html-attribute-props v1.4.1
 *
 * Copyright (c) 2024, Brandon D. Sara (https://bsara.dev/)
 * Licensed under the ISC license (https://gitlab.com/bsara/pick-html-attribute-props/blob/master/LICENSE)
 */

import {
  getFormHtmlAttrNames,
  getGlobalHtmlAttrNames,
  getInputHtmlAttrNames,
  getTextAreaHtmlAttrNames
} from './attr-names';

import {
  propertyIsDataOrAriaAttr,
  propertyIsDataOrAriaAttrOrEventHandler
} from './prop-name-predicates';



/**
 * Creates a new object composed of properties "picked" from `obj` that are valid HTML
 * Form attributes or are event handlers _(unless specified otherwise in `options`)_
 * found in the given `obj`. If `obj` is not defined, then an empty object is returned.
 *
 * @param {?object}  obj - Object to be used when searching for HTML attribute values.
 *
 * @param {?object}  [options]
 * @param {?boolean} [options.omitEventHandlers = false] - when `true`, event handlers
 *                                                         will be omitted from the
 *                                                         returned object.
 *
 * @returns {!object} A new object composed of only those properties that were
 *                    determined to be valid HTML Form attributes or are event handlers
 *                    _(unless `options.omitEventHandlers` is `true`, then all event
 *                    handlers will be omitted form the returned object)_. If none are
 *                    found, then an empty object is returned.
 */
export function pickFormHtmlAttrProps(obj, options) {
  return _pickAttributes(obj, getFormHtmlAttrNames(), options);
}



/**
 * Creates a new object composed of properties "picked" from `obj` that are valid HTML
 * attributes that can be supplied to any HTML element or are event handlers _(unless
 * specified otherwise in `options`)_ found in the given `obj`. If `obj` is not defined,
 * then an empty object is returned.
 *
 * @param {?object}  obj - Object to be used when searching for HTML attribute values.
 *
 * @param {?object}  [options]
 * @param {?boolean} [options.omitEventHandlers = false] - when `true`, event handlers
 *                                                         will be omitted from the
 *                                                         returned object.
 *
 * @returns {!object} A new object composed of only those properties that were determined
 *                    to be valid, globally applicable HTML attributes or are event
 *                    handlers _(unless `options.omitEventHandlers` is `true`, then all
 *                    event handlers will be omitted form the returned object)_. If none
 *                    are found, then an empty object is returned.
 */
export function pickGlobalHtmlAttrProps(obj, options) {
  return _pickAttributes(obj, getGlobalHtmlAttrNames(), options);
}



/**
 * Creates a new object composed of properties "picked" from `obj` that are valid HTML
 * Input attributes or are event handlers _(unless specified otherwise in `options`)_
 * found in the given `obj`. If `obj` is not defined, then an empty object is returned.
 *
 * @param {?object}  obj - Object to be used when searching for HTML attribute values.
 *
 * @param {?object}  [options]
 * @param {?boolean} [options.omitEventHandlers = false] - when `true`, event handlers
 *                                                         will be omitted from the
 *                                                         returned object.
 *
 * @returns {!object} A new object composed of only those properties that were
 *                    determined to be valid HTML Input attributes or are event handlers
 *                    _(unless `options.omitEventHandlers` is `true`, then all event
 *                    handlers will be omitted form the returned object)_. If none are
 *                    found, then an empty object is returned.
 */
export function pickInputHtmlAttrProps(obj, options) {
  return _pickAttributes(obj, getInputHtmlAttrNames(), options);
}



/**
 * Creates a new object composed of properties "picked" from `obj` that are valid HTML
 * TextArea attributes or are event handlers _(unless specified otherwise in `options`)_
 * found in the given `obj`. If `obj` is not defined, then an empty object is returned.
 *
 * @param {?object}  obj - Object to be used when searching for HTML attribute values.
 *
 * @param {?object}  [options]
 * @param {?boolean} [options.omitEventHandlers = false] - when `true`, event handlers
 *                                                         will be omitted from the
 *                                                         returned object.
 *
 * @returns {!object} A new object composed of only those properties that were
 *                    determined to be valid HTML TextArea attributes or are event
 *                    handlers _(unless `options.omitEventHandlers` is `true`, then all
 *                    event handlers will be omitted form the returned object)_. If none
 *                    are found, then an empty object is returned.
 */
export function pickTextAreaHtmlAttrProps(obj, options) {
  return _pickAttributes(obj, getTextAreaHtmlAttrNames(), options);
}



// region Private Helpers

/** @private */
function _pickAttributes(obj, propNames, options) {
  if (obj == null) {
    return {};
  }

  return Object.assign(
    {},
    _pick(obj, propNames),
    _pickDataAttrsAriaAttrsAndEventHandlers(obj, options)
  );
}


/** @private */
function _pickDataAttrsAriaAttrsAndEventHandlers(obj, { omitEventHandlers } = {}) {
  return _pickBy(obj, (omitEventHandlers ? propertyIsDataOrAriaAttr : propertyIsDataOrAriaAttrOrEventHandler));
}


/** @private */
function _pick(obj, propNames) {
  if (obj == null) {
    return {};
  }

  const ret = {};

  for (let i = 0; i < propNames.length; i++) {
    const propName = propNames[i];

    if (Object.prototype.hasOwnProperty.call(obj, propName)) {
      ret[propName] = obj[propName];
    }
  }

  return ret;
}


/** @private */
function _pickBy(obj, predicate) {
  if (obj == null) {
    return {};
  }

  const ret = {};

  for (const propName in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, propName) && predicate(propName)) {
      ret[propName] = obj[propName];
    }
  }

  return ret;
}

// endregion
