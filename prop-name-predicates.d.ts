/**!
 * pick-html-attribute-props v1.4.1
 *
 * Copyright (c) 2024, Brandon D. Sara (https://bsara.dev/)
 * Licensed under the ISC license (https://gitlab.com/bsara/pick-html-attribute-props/blob/master/LICENSE)
 */

/**
 * @return {boolean} `true` if the provided `propName` likely represents an event
 *                   handler, data HTML attribute, or aria HTML attribute; otherwise,
 *                   `false`.
 */
export function propertyIsDataOrAriaAttrOrEventHandler(propName: string | null): boolean;



/**
 * @return {boolean} `true` if the provided `propName` likely represents a data or aria
 *                   HTML attribute; otherwise, `false`.
 */
export function propertyIsDataOrAriaAttr(propName: string | null): boolean;



/**
 * @return {boolean} `true` if the provided `propName` likely represents an event
 *                   handler; otherwise, `false`.
 */
export function propertyIsEventHandler(propName: string | null): boolean;
