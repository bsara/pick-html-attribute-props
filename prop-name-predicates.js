// eslint-disable-next-line spaced-comment
/**!
 * pick-html-attribute-props v1.4.1
 *
 * Copyright (c) 2024, Brandon D. Sara (https://bsara.dev/)
 * Licensed under the ISC license (https://gitlab.com/bsara/pick-html-attribute-props/blob/master/LICENSE)
 */

// region Private Constants

const EVENT_HANDLER_REGEX = /^on[A-Z]/;

// endregion



/**
 * @return {boolean} `true` if the provided `propName` likely represents an event
 *                   handler, data HTML attribute, or aria HTML attribute; otherwise,
 *                   `false`.
 */
export function propertyIsDataOrAriaAttrOrEventHandler(propName) {
  return (
    typeof propName === 'string'
      && (propertyIsEventHandler(propName) || propertyIsDataOrAriaAttr(propName))
  );
}


/**
 * @return {boolean} `true` if the provided `propName` likely represents an event
 *                   handler, data HTML attribute, or aria HTML attribute; otherwise,
 *                   `false`.
 */
export function propertyIsDataOrAriaAttr(propName) {
  return (
    typeof propName === 'string'
      && (propName.startsWith('data-') || propName.startsWith('aria-'))
  );
}


/**
 * @return {boolean} `true` if the provided `propName` likely represents an event
 *                   handler; otherwise, `false`.
 */
export function propertyIsEventHandler(propName) {
  return (
    typeof propName === 'string'
      && propName.match(EVENT_HANDLER_REGEX) != null
  );
}
