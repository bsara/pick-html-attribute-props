# 1.4.0

- **[NEW FEATURE]** Added more specific Typescript types


# 1.3.2

- **[NEW FEATURE]** Added form element attributes
- **[NEW FEATURE]** Added types files
- **[NEW FEATURE]** Added inline docs
- Updated to latest dependencies


# 1.2.3

- Updated to latest dev dependencies


# 1.2.2

- Updated to latest dev dependencies and updated license year


# 1.2.1

- **[BUG FIX]** Props weren't being properly picked.


# 1.2.0

- **[NEW FEATURE]** Updated package to include node and browser compatible versions of library.


# 1.0.1

- Forgot to add description to `package.json` in last release. woops!


# 1.0.0

- Initial release.
